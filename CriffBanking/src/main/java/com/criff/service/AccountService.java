package com.criff.service;



import java.util.List;
import com.criff.curtis.Main;
import com.criff.exceptions.WithdrawalException;
import com.criff.exceptions.DepositException;
import com.criff.model.Account;
import com.criff.model.Transaction;
import com.criff.repository.AccountRepository;
import com.criff.repository.TransactionRepository;
import com.criff.repository.UserRepository;

public class AccountService {

    private AccountRepository accountRepository;
    private TransactionRepository transactionRepository;
    private UserRepository userRepository;

    public AccountService() { }

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public AccountService(AccountRepository accountRepository, TransactionRepository transactionRepository, UserRepository userRepository) {
        this.accountRepository = accountRepository;
        this.transactionRepository = transactionRepository;
        this.userRepository = userRepository;
    }

    public void showAccounts() {

        int userId = UserService.currentUser.getUserId();
        List<Account> accounts = this.accountRepository.getAllAccounts(userId);
        if (accounts == null || accounts.size() == 0) {
        	displayHeader("There are no accounts to show");
        } else {
            for (final Account account : accounts) {
            	displayHeader("Account ID: " + Account.getAccountId() + "    Account UserUser ID: " + account.getAccountUserId() +
                        "  Account Type: " + account.getAccountType() + "    Balance: $" + account.getBalance());
            }
        }
    }
    

    public void makeWithdrawal() throws WithdrawalException {
    	displayHeader("How much would you like to withdraw?");
        double amount = Main.scan.nextDouble();
        displayHeader("Which account would you like to withdraw from?");
        showAccounts();
        displayHeader("Enter account ID of the account you would like to withdraw from.");
        int accountId = Main.scan.nextInt();
        try {
            Account a = this.accountRepository.getById(accountId);
            int userId = UserService.currentUser.getUserId();
            if (a.getAccountUserId() != userId) {
            	displayHeader("Invalid account for current user. Please try again.");
            } else {
                double attemptedBalance = a.getBalance() - amount;
                if (attemptedBalance < 0) {
                    throw new WithdrawalException();
                } else {
                    a.setBalance(attemptedBalance);
                    this.accountRepository.updateBalance(a);
                    Transaction t = new Transaction(accountId, -amount);
                    this.transactionRepository.newTransaction(t);
                    displayHeader("\nWithdrawal successful!");
                }
                displayHeader("Account ID: " + Account.getAccountId() + "    Account Type: " + a.getAccountType()
                        + "    Balance: $" + a.getBalance());
            }
        } catch (NullPointerException ne) {
        	displayHeader("\nAccount not found. Please try again.");
        }
    }

    public void makeDeposit() throws DepositException {
    	displayHeader("How much would you like to deposit?");
        double amount = Main.scan.nextDouble();
        displayHeader("Which account would you like to deposit to?");
        showAccounts();
        displayHeader("Enter account ID of the account you would like to deposit to.");
        int accountId = Main.scan.nextInt();
        try {
            if (amount > 10000) {
                throw new DepositException();
            } else {
                Account a = this.accountRepository.getById(accountId);
                int userId = UserService.currentUser.getUserId();
                if (a.getAccountUserId() != userId) {
                	displayHeader("Invalid account for current user. Please try again.");
                } else {
                    a.setBalance(a.getBalance() + amount);
                    this.accountRepository.updateBalance(a);
                    Transaction t = new Transaction(accountId, amount);
                    this.transactionRepository.newTransaction(t);
                    displayHeader("\nDeposit successful!");
                    displayHeader("Account ID: " + Account.getAccountId() + "    Account Type: " + a.getAccountType() + "    Balance: $" + a.getBalance());
                }
            }
        } catch (NullPointerException ne) {
        	displayHeader("\nAccount not found. Please try again.");
        }
    }

    public void transfer() throws WithdrawalException {
    	displayHeader("How much would you like to transfer?");
        double amount = Main.scan.nextDouble();
        showAccounts();
        displayHeader("Which account would you like to transfer from?");
        displayHeader("Enter account ID of the account you would like to transfer from.");
        int from = Main.scan.nextInt();
        displayHeader("Which account would you like to transfer to?");
        displayHeader("Enter account ID of the account you would like to transfer to.");
        int to = Main.scan.nextInt();
        if (from == to) {
        	displayHeader("\nYou must choose 2 different accounts. Please try again.");
        } else {
            try {
                Account w = this.accountRepository.getById(from);
                Account d = this.accountRepository.getById(to);
                int userId = UserService.currentUser.getUserId();
                if (w.getAccountUserId() != userId || d.getAccountUserId() != userId) {
                	displayHeader("One or more account entries are invalid. Please try again.");
                } else {
                    double attemptedBalance = w.getBalance() - amount;
                    if (attemptedBalance < 0) {
                        throw new WithdrawalException();
                    } else {
                        w.setBalance(attemptedBalance);
                        this.accountRepository.updateBalance(w);
                        Transaction tw = new Transaction(from, -amount);
                        this.transactionRepository.newTransaction(tw);
                        d.setBalance(d.getBalance() + amount);
                        this.accountRepository.updateBalance(d);
                        Transaction td = new Transaction(to, amount);
                        this.transactionRepository.newTransaction(td);
                        displayHeader("\nTransfer successful!");
                        displayHeader("Account ID: " + Account.getAccountId() + "    Account Type: " + w.getAccountType() + "    Balance: $" + w.getBalance());
                        displayHeader("Account ID: " + Account.getAccountId() + "    Account Type: " + d.getAccountType() + "    Balance: $" + d.getBalance());
                    }
                }
            } catch (NullPointerException ne) {
            	displayHeader("\nOne or more account(s) not found. Please try again.");
            }
        }
    }
    
    private void displayHeader(String message){
        System.out.println();
        int width = message.length() + 6;
        StringBuilder sb = new StringBuilder();
        sb.append("+");
        for(int i = 0; i < width; ++i){
            sb.append("-");
        }
        sb.append("+");
        System.out.println(sb.toString());
        System.out.println("|   " + message + "   |");
        System.out.println(sb.toString());
    }

}
