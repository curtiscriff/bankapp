package com.criff.service;



import java.util.List;

import com.criff.curtis.Main;
import com.criff.model.Transaction;
import com.criff.repository.TransactionRepository;

public class TransactionService {

    public TransactionRepository transactionDao;

    public TransactionService() {}

    public TransactionService(TransactionRepository transactionDao) { this.transactionDao = transactionDao; }

    public List<Transaction> showTransactions() {
    	displayHeader("Enter Account ID of the account transactions you'd like to view");
        int accountId = Main.scan.nextInt();
        List<Transaction> transactions = this.transactionDao.getAllTransactions(accountId);
        if (transactions == null || transactions.size() == 0) {
        	displayHeader("There are no transactions to show");
        } else {
            for (final Transaction transaction : transactions) {
            	displayHeader(" Account ID: " + transaction.getAccountId() +
                        " Transaction Amount: $" + transaction.getAmount());
            }
        }
        return transactions;
    }
    
    public static void displayHeader(String message){
        System.out.println();
        int width = message.length() + 6;
        StringBuilder sb = new StringBuilder();
        sb.append("+");
        for(int i = 0; i < width; ++i){
            sb.append("-");
        }
        sb.append("+");
        System.out.println(sb.toString());
        System.out.println("|   " + message + "   |");
        System.out.println(sb.toString());
    }
}
