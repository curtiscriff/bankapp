package com.criff.service;



import com.criff.curtis.Main;
import com.criff.model.Account;
import com.criff.model.Checking;
import com.criff.model.Savings;
import com.criff.model.User;
import com.criff.repository.UserRepository;

public class UserService {

    private UserRepository userRepository;
    public boolean loggedIn = false;
    public static User currentUser = null;

    public UserService() { }

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private boolean authenticate(String username, String password) {
        User attemptedUser = this.userRepository.getUserByUsername(username);
        if(attemptedUser == null) {
            return false;
        } else {
            if(attemptedUser.getPassword().equals(password)) {
                loggedIn = true;
                return true;
            }
        }
        return false;
    }

    public void login() {
        loggedIn = false;
        while (!loggedIn) {
        	System.out.print("\nEnter First Name: ");
            String fn = Main.scan.next();
            System.out.print("Enter username: ");
            String un = Main.scan.next();
            System.out.print("Enter password: ");
            String pw = Main.scan.next();
            if (authenticate(un, pw)) {
            	
//            	if (un.equalsIgnoreCase("Admin") && pw.equals("1234") || (un.equalsIgnoreCase("emp") && pw.equals("1234"))) {
//            		
//            		System.out.println("Hello Admin");
//                    //runMenu4();
//                }
//            	{
//            		//runMenu3();
//            	}
            	
            	
                currentUser = this.userRepository.getUserByUserFirstName(fn);
                displayHeader("Login successful!");
                displayHeader("Welcome " + fn + "!");
                loggedIn = true;
            } else {
            	displayHeader("Email or password incorrect. Please try again");
            }
        }
    }

    public void logout() {
        boolean logIn = false;
        currentUser = null;
        displayHeader("You have logged out.");
        displayHeader("Enter 1 to login again.");
        String choice = Main.scan.next();
        while (logIn == false) {
            if (choice.equals("1")) {
                login();
                logIn = true;
            } else {
            	displayHeader("Please enter 1 to login and view available actions.");
                choice = Main.scan.next();
                logIn = false;
            }
        }

    }
    
    public void createAccount() {
		String firstName, lastName, ssn, username = "", password = "", jointAccount = "", jointusername = "", jointpassword = "", accountType = "";
		int userid = 0;
		double initialDeposit = 0;
		boolean valid = false;
		boolean isEmpty = false;
		while(!valid) {
			System.out.print("Please Enter An Account Type: (1) Checking (2) Savings): ");
			accountType = Main.scan.nextLine();
			if(accountType.matches("1") || accountType.matches("2")) {
				valid = true;				
			}
			else {
				System.out.println("Invalid Account Type Was Selected. Please Enter Checking or Savings. ");
			}
		}
		
			System.out.print("Will This Be A Joint Account (1) Yes (2) No?: \n");
			jointAccount = Main.scan.nextLine();
			if(jointAccount.matches("1")) {
				
				jointAccount = "Yes";
				System.out.print("Please Enter Joint Account Holder First Name: ");				
				jointusername = Main.scan.nextLine();
				
				System.out.print("Please Enter Joint Account Holder Last Name: ");				
				jointpassword = Main.scan.nextLine();
				
				
			}
			else if(jointAccount.matches("2")){
				
				jointAccount = "No";
				
			} else {
				
				System.out.println("Invalid Account Type Was Selected. Please Enter Checking or Savings. ");
			}
		
		
		
		
		System.out.print("Please Enter Primary Account Holder First Name: ");
		
		firstName = Main.scan.nextLine();
		
		
		do {
			// Validate if User left First name empty or just entered spaces, which means it is empty. 
			if (firstName == null || firstName.trim().isEmpty()) {
				System.out.print("\nYou Can't Leave This Field Empty.\n" + "\n" + "Please Enter Your First Name: ");

				firstName = Main.scan.nextLine();
			// Validate if User types numeric values as First Name.	
			} else if (firstName.matches("[0-9]+")) {
				System.out.print("\nYou Can't Have Digits In Your Name.\n" + "\n" + "Please Enter Your First Name: ");

				firstName = Main.scan.nextLine();
			// Validate if user types numbers and letters or letters and numbers for First Name.	
			} else if (firstName.matches("[a-zA-Z]+[0-9]+") || firstName.matches("[0-9]+[a-zA-Z]+")) {
				System.out.print("\nYou Can't Have Both Digits And Characters In Your Name.\n" + "\n" + "Please Enter Your First Name: ");

				firstName = Main.scan.nextLine();
				
			} else {
				
				isEmpty = true;
				
			}
		} while (!(isEmpty));
		
		
		System.out.print("Please Enter Primary Account Holder Last Name: ");
		
		lastName = Main.scan.nextLine();
		
		isEmpty = false;
		do {
			// Validate if User left Last name empty or just entered spaces, which means it is empty. 
			if (lastName == null || lastName.trim().isEmpty()) {
				System.out.print("\nYou Can't Leave This Field Empty.\n" + "\n" + "Please Enter Your Last Name: ");

				lastName = Main.scan.nextLine();
			// Validate if User types numeric values as Last Name.	
			} else if (lastName.matches("[0-9]+")) {
				System.out.print("\nYou Can't Have Digits In Your Name.\n" + "\n" + "Please Enter Your Last Name: ");

				lastName = Main.scan.nextLine();
			// Validate if user types numbers and letters or letters and numbers for Last Name.	
			} else if (lastName.matches("[a-zA-Z_0-9]+[0-9]+") || lastName.matches("[0-9]+[a-zA-Z]+")) {
				System.out.print("\nYou Can't Have Both Digits And Characters In Your Name.\n" + "\n" + "Please Enter Your Last Name: ");

				lastName = Main.scan.nextLine();
				
			} else {
				
				isEmpty = true;
				
			}
		} while (!(isEmpty));
		
		
		System.out.print("Please Enter Your Social Security Number: ");
		
		ssn = Main.scan.nextLine();
		
		isEmpty = false;
		do {
			// Validate if User left ssn empty or just entered spaces, which means it is empty. 
			if (ssn == null || ssn.trim().isEmpty()) {
				System.out.print("\nYou Can't Leave This Field Empty.\n" + "\n" + "Please Enter Your Social Security Number: ");

				ssn = Main.scan.nextLine();
				
			// Validate if User types numeric values as ssn.	
			} else if (ssn.matches("[a-zA-Z]+")) {
				System.out.print("\nYou Can't Have Letters In Your Social Security Number.\n" + "\n" + "Please Enter Your Social Security Number: ");

				ssn = Main.scan.nextLine();
			// Validate if user types numbers and letters or letters and numbers for ssn.	
			} else if (lastName.matches("[a-zA-Z]+[0-9]+") || ssn.matches("[0-9]+[a-zA-Z]+")) {
				System.out.print("\nYou Can't Have Both Digits And Characters In Your Social Security Number.\n" + "\n" + "Please Enter Your Social Security Number: ");

				ssn = Main.scan.nextLine();
				
			} else {
				
				isEmpty = true;
				
			}
		} while (!(isEmpty));
		
		System.out.print("Please Enter Your Username: ");
		
		username = Main.scan.nextLine();
		
		System.out.print("Please Enter Your Password: ");
		
		password = Main.scan.nextLine();
		
		valid = false;
		while(!valid) {
			System.out.print("Please Enter An Initial Deposit: ");			
			try {
				initialDeposit = Double.parseDouble(Main.scan.nextLine());
			}
			catch(NumberFormatException e){
				System.out.println("Deposit Must Be A Number");
			}
			if(accountType.matches("1")) {
				if(initialDeposit < 100) {
					System.out.println("Checking Accounts Require A Minimum of $100 To Open.");
				} else {
					valid = true;
				}
			} else if(accountType.matches("2")) {
				if(initialDeposit < 50) {
					System.out.println("Savings Accounts Require A Minimum of $50 To Open.");
				} else {
					valid = true;
					System.out.println("\n");
				}
			}
		}
		
		// create accounts
		Account account;
		if(accountType.matches("1")) {
			account = new Checking(initialDeposit);
		} 
		else {
			account = new Savings(initialDeposit);
		}
		
		User users = new User(userid, firstName, lastName, ssn, username, password, jointAccount, jointusername, jointpassword, account);
		//account(users);
		System.out.println("\nYour Account Has Been Created Successfully!\n");
		System.out.println(account); // Prints out basic account info after the account has been created
		
	}
    
   public static void displayHeader(String message){
        System.out.println();
        int width = message.length() + 6;
        StringBuilder sb = new StringBuilder();
        sb.append("+");
        for(int i = 0; i < width; ++i){
            sb.append("-");
        }
        sb.append("+");
        System.out.println(sb.toString());
        System.out.println("|   " + message + "   |");
        System.out.println(sb.toString());
    }
}
