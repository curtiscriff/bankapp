package com.criff.main;

import java.sql.SQLException;
import java.util.InputMismatchException;
import com.criff.curtis.Main;
import com.criff.exceptions.WithdrawalException;
import com.criff.exceptions.DepositException;
import com.criff.repository.AccountRepositoryImpl;
import com.criff.repository.AccountRepository;
import com.criff.repository.TransactionRepository;
import com.criff.repository.UserRepository;
import com.criff.repository.TransactionRepositoryImpl;
import com.criff.repository.UserRepositoryImpl;
import com.criff.service.AccountService;
import com.criff.service.TransactionService;
import com.criff.service.UserService;
import com.criff.utility.ConnectionUtil;

public class Menu {
    private UserService userService;
    private AccountService accountService;
    private TransactionService transactionService;

    public void run (String... args) {
        try {
            init();
            userService.login();

            boolean quit = false;
            do {
            	displayHeader("***** Please Make A Choice From The Menu Below *****");
            	System.out.println("");
            	System.out.println("( 1.) Check Account Balances   ( 5.) View Past Transactions");
            	System.out.println("( 2.) Make A Withdrawal        ( 6.) Create A New Account");
            	System.out.println("( 3.) Make A Deposit           ( 7.) Logout");
            	System.out.println("( 4.) Transfer Funds           ( 8.) Quit the program");
                displayHeader("          ***** Make Your Selection *****           ");

                try {
                    int choice = Main.scan.nextInt();
                    switch (choice) {
                        case 1: // check balances
                            accountService.showAccounts();
                            break;
                        case 2: // withdrawal
                            try {
                                accountService.makeWithdrawal();
                            } catch (WithdrawalException e) {}
                            break;
                        case 3: // deposit
                            try {
                                accountService.makeDeposit();
                            } catch (DepositException e) {}
                            break;
                        case 4: // transfer
                            try {
                                accountService.transfer();
                            } catch (WithdrawalException e) {}
                            break;
                        case 5: // past transactions
                            transactionService.showTransactions();
                            break;
                        case 6: // create new account
                            userService.createAccount();
                            break;
                        case 7: // logout
                            userService.logout();
                            break;
                        case 8: // quit
                            quit = true;
                            displayHeader("Thank You For Banking With Us. Have A Nice Day!");
                            break;
                        default:
                        	displayHeader("Invalid Choice. Please Choose Again.");
                            break;
                    }
                } catch (InputMismatchException e) {
                	displayHeader("Invalid Entry Type. Please Try Again.");
                    Main.scan.next();
                }
            } while (quit == false) ;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void init() throws SQLException {
        ConnectionUtil connectionUtil = new ConnectionUtil("jdbc:postgresql://criffbanking.cua8a0jy3iil.us-east-2.rds.amazonaws.com/criffbanking",
                "postgres", "lost1soul", new org.postgresql.Driver());

        UserRepository userRepository = new UserRepositoryImpl(connectionUtil);
        userService = new UserService(userRepository);

        TransactionRepository transactionRepository = new TransactionRepositoryImpl(connectionUtil);
        transactionService = new TransactionService(transactionRepository);

        AccountRepository accountRepository = new AccountRepositoryImpl(connectionUtil);
        accountService = new AccountService(accountRepository, transactionRepository, userRepository);
        
        
        
        
        

        System.out.println("\n  ____              _    _               ____            _         \n"
                + " | __ )  __ _ _ __ | | _(_)_ __   __ _  / ___| _   _ ___| |_ ___ _ __ ___   \n"
                + " |  _ \\ / _` | '_ \\| |/ / | '_ \\ / _` | \\___ \\| | | / __| __/ _ \\ '_ ` _  |\n"
                + " | |_) | (_| | | | |   <| | | | | (_| |  ___) | |_| \\__ \\ ||  __/ | | | | | \n"
                + " |____/ \\__,_|_| |_|_|\\_\\_|_| |_|\\__, | |____/ \\__, |___/\\__\\___|_| |_| |_| \n"
                + "                                 |___/         |___/             \n");

        displayHeader("WELCOME TO CRIFF BANKING SYSTEM! PRESS ENTER TO LOGIN");
        //Scanner scan = new Scanner(System.in);
        Main.scan.nextLine();
        displayHeader("LOGIN");
    }
    
    
    private void displayHeader(String message){
        System.out.println();
        int width = message.length() + 6;
        StringBuilder sb = new StringBuilder();
        sb.append("+");
        for(int i = 0; i < width; ++i){
            sb.append("-");
        }
        sb.append("+");
        System.out.println(sb.toString());
        System.out.println("|   " + message + "   |");
        System.out.println(sb.toString());
    }

}
