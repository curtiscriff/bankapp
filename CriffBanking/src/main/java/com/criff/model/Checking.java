package com.criff.model;


public class Checking extends Account{
    private static String accountType = "Checking";
    
    public Checking(double initialDeposit){
        this.setBalance(initialDeposit);
    }
    
    @Override
    public String toString(){
        return "Account Type: " + accountType + " Account\n" +
                "Account Number: " + Account.getAccountId() + "\n" +
                "Balance: " + this.getBalance() + "\n";

    }




}
