package com.criff.model;

public enum AccountType {
	
	Undefined,
	Checking,
	Savings

}
