package com.criff.model;

public class Account {
    private static int accountId;
    private int accountUserId;
    private String accountType;
    private double balance;

    
    public Account(int accountId, int accountUserId, String accountType, double balance, String accountNumber) {
        Account.accountId = accountId;
        this.accountUserId = accountUserId;
        this.accountType = accountType;
        this.balance = balance;
    }
    
    public Account() {}
    
    
    public static int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        Account.accountId = accountId;
    }

    public int getAccountUserId() {
        return accountUserId;
    }

    public void setAccountHolderId(int accountHolderId) {
        this.accountUserId = accountHolderId;
    }

    public double getBalance() {
        return balance;
    }

	/**
	 * @param accountType the accountType to set
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountId=" + accountId +
                ", accountUserId=" + accountUserId +
                ", accountType='" + accountType + '\'' +
                ", balance=" + balance +
                '}';
    }

	/**
	 * @return the accountType
	 */
	public String getAccountType() {
		return accountType;
	}
}
