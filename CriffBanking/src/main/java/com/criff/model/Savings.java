package com.criff.model;



public class Savings extends Account{
    private static String accountType = "Savings";
    
    public Savings(double initialDeposit){
        this.setBalance(initialDeposit);
    }
    
    @Override
    public String toString(){
        return "Account Type: " + accountType + " Account\n" +
                "Account Number: " + Account.getAccountId() + "\n" +
                "Balance: " + this.getBalance() + "\n";
    }


}
