package com.criff.model;

public class User {
    private int userId;
    private String firstname;
    private String lastname;
    private String ssn;
    private String username;
    private String password;
    private String jointusername;
    private String jointpassword;
    private Account account;

   

    /**
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(Account account) {
		this.account = account;
	}

	public User(int userId, String firstname, String lastname, String ssn, String username, String password, String jointAccount, String jointusername, String jointpassword, Account account) {
        this.userId = userId;
        this.firstname = firstname;
        this.lastname = lastname;
        this.ssn = ssn;
        this.username = username;
        this.password = password;
        this.jointusername = jointusername;
        this.jointpassword = jointpassword;
    }
    
    public User() {}

    /**
	 * @return the ssn
	 */
	public String getSsn() {
		return ssn;
	}

	/**
	 * @param ssn the ssn to set
	 */
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	/**
	 * @return the firstName
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * @return the lastName
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	 @Override
	    public String toString() {
	        return "User{" +        		
	                "userId="       + userId +
	                ", firstname='" + firstname + '\'' +
	                ", lastname='"  + lastname + '\'' +
	                ", ssn='"       + ssn + '\'' +
	                ", username='"  + username + '\'' +
	                ", password='"  + password + '\'' +
	                ", jointusername='"  + jointusername + '\'' +
	                ", jointpassword='"  + jointpassword + '\'' +
	                ", account='"   + account + '\'' +
	                '}';
	    }

	public void add(User user) {
		// TODO Auto-generated method stub
		
	}

}


