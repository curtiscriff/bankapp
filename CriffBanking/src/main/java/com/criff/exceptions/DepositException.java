package com.criff.exceptions;

public class DepositException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DepositException() {
        System.out.println("Deposit Amount Exceeds Maximum For CRIFF BANKING. Please Visit A Local Branch To Complete This Transaction.");
    }
}
