package com.criff.exceptions;

public class WithdrawalException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public WithdrawalException() {
        System.out.println("Withdraw Cannot Be Completed. Insufficient Funds.");
    }
}
