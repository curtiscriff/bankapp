package com.criff.repository;

import java.util.List;

import com.criff.model.Account;
import com.criff.model.User;



/*
 * Your repository layer serves as your data layer. This is the layer of
 * your application which should directly interact with your database. In
 * other words, we're separating our concerns.
 * 
 * We will use this interface to define methods that must be implemented
 * by classes which interact with our database.
 */


public interface BankRepository {
	
	List<User> getAllUsers();
	User getUserById(int userid);
	void insertUser(User user);
	void updateUserById(int userid);
	void deleteUserById(int userid);
	
	List<Account> getAllAccounts();
	Account getAccountById(int userid);
	void insertAccount(Account account);
	void updateAccountById(int userid);
	void deleteAccountById(int userid);
	
//	List<Employee> getAllEmployees();
//	Employee getEmployeeById(int id);
//	void insertEmployee(Employee employee);
//	void updateEmployeeById(int id);
//	void deleteEmployeeById(int id);
//	
//	List<Manager> getAllManagers();
//	Manager getManagerById(int id);
//	void insertManager(Manager manager);
//	void updateManagerById(int id);
//	void deleteManagerById(int id);
	
}
