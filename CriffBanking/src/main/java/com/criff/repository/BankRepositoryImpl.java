package com.criff.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.criff.model.Account;
import com.criff.model.User;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.core.Logger;

import com.criff.utility.ConnectionClosers;
import com.criff.utility.ConnectionFactory;



/*
 * This is our data access object (DAO). It is an object which serves the
 * purpose of interacting with our database.
 */

/*
 * Log4J is a logging tool which allows us to log to our console, a file, etc.
 * This is preferred to using sysout as we there is a business-orient advantage to 
 * producing logs.
 */





public class BankRepositoryImpl implements BankRepository {
	
	//private static final Logger log  = LogManager.getLogger(BankRepositoryImpl.class);
	
	@Override
	public List<User> getAllUsers() {
	
	/*
	 * This method will return all of the Users that are currently in my
	 * database. To start, we'll need a connection (and, of course, an
	 * implementation of the List interface that will hold all of the records we get
	 * back from our DB).
	 */
	
	List<User> users = null;

	Connection conn = null;
	/*
	 * I will need a statement if I want to execute a SQL statement.
	 */
	Statement stmt = null;
	/*
	 * I need a result set to hold the records that will be returned.
	 */
	ResultSet set = null;
	
	final String SQL = "select * from Users";
	
	


	try {
	
		conn = ConnectionFactory.getConnection();
		stmt = conn.createStatement();
		set = stmt.executeQuery(SQL);
		users = new ArrayList<User>();

		while (set.next()) {
			users.add(
					/*
					 * The indices start at 1 (not 0) when accessing the columns on a table in a
					 * database.
					 */
					new User(set.getInt(1),
							set.getString(2), 
							set.getString(3),
							set.getString(4),
							set.getString(5), SQL, SQL, SQL, SQL, null
					));

		}
		
	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		ConnectionClosers.closeConnection(conn);
		ConnectionClosers.closeStatement(stmt);
		ConnectionClosers.closeResultSet(set);
	}

	return users;
}
		
		@Override
		public User getUserById(int userid) {
			User user = null;

			Connection conn = null;
			PreparedStatement stmt = null;
			ResultSet set = null;
			
			/*
			 * This time around, we are taking user input and using it to construct
			 * a query. As a result, we will be vulnerable to SQL injection if
			 * we don't use a PreparedStatement. In order to use a PreparedStatement,
			 * we can parameterize our query by using the "?" as a placeholder and then
			 * set a value for this question mark later.
			 */
			final String SQL = "select * from users where userid = ?";

			try {
				conn = ConnectionFactory.getConnection();
				stmt = conn.prepareStatement(SQL);
				/*
				 * Note that the parameter indices start from 1. A parameter
				 * index refers to a question mark you have used to parameterize
				 * a query.
				 */
				stmt.setInt(1, userid);
				set = stmt.executeQuery();
				
				while(set.next()) {
					user = new User(set.getInt(1),
							set.getString(2), 
							set.getString(3),
							set.getString(4), 
							set.getString(5), SQL, SQL, SQL, SQL, null
							);
				}
				
				/*
				 * Now we have to set the parameters.
				 */
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				ConnectionClosers.closeConnection(conn);
				ConnectionClosers.closeStatement(stmt);
				ConnectionClosers.closeResultSet(set);
			}

			return user;
		}
		
		

		@Override
		public void insertUser(User user) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateUserById(int userid) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void deleteUserById(int userid) {
			// TODO Auto-generated method stub
			
		}



		@Override
		public List<Account> getAllAccounts() {

			/*
			 * This method will return all of the Customers that are currently in my
			 * database. To start, we'll need a connection (and, of course, an
			 * implementation of the List interface that will hold all of the records we get
			 * back from our DB).
			 */
			
			List<Account> accounts = null;

			Connection conn = null;
			/*
			 * I will need a statement if I want to execute a SQL statement.
			 */
			Statement stmt = null;
			/*
			 * I need a result set to hold the records that will be returned.
			 */
			ResultSet set = null;
			
			final String SQL = "select * from Accounts";
			
			


			try {
			
				conn = ConnectionFactory.getConnection();
				stmt = conn.createStatement();
				set = stmt.executeQuery(SQL);
				accounts = new ArrayList<Account>();

				while (set.next()) {
					accounts.add(
							/*
							 * The indices start at 1 (not 0) when accessing the columns on a table in a
							 * database.
							 */
							new Account(set.getInt(1),
									set.getInt(2), 
									set.getString(3),
									set.getDouble(4),
									set.getString(5)
							));

				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				ConnectionClosers.closeConnection(conn);
				ConnectionClosers.closeStatement(stmt);
				ConnectionClosers.closeResultSet(set);
			}

			return accounts;
			
		}

		@Override
		public Account getAccountById(int id) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void insertAccount(Account account) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateAccountById(int id) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void deleteAccountById(int id) {
			// TODO Auto-generated method stub
			
		}
	}