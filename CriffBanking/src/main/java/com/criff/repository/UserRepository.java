package com.criff.repository;

import com.criff.model.User;

public interface UserRepository {
    User getUserByUsername(String username);

	User getUserByUserID(String userid);

	User getUserByUserFirstName(String firstname);

	void newUser(User user);
}
