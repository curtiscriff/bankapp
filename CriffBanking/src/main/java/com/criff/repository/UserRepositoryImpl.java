package com.criff.repository;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.criff.model.Account;
import com.criff.model.User;
import com.criff.utility.ConnectionUtil;

public class UserRepositoryImpl implements UserRepository {

    private ConnectionUtil connectionUtil;

    public UserRepositoryImpl() {

    }

    public UserRepositoryImpl(ConnectionUtil connectionUtil) {
        this.connectionUtil = connectionUtil;
    }


    @Override
    public User getUserByUsername(String username) {
        Connection c = null;
        User u = null;

        try {
            c = this.connectionUtil.newConnection();
            c.setAutoCommit(false);
            String sql = "Select userid, firstname, lastname, username, pw from users where username = ?";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, username);

            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
                u = new User();
                u.setUserId(rs.getInt("userid"));
                u.setFirstname(rs.getString("firstname"));
                u.setLastname(rs.getString("lastname"));
                u.setUsername(rs.getString("username"));
                u.setPassword(rs.getString("pw"));
            }

            c.commit();
            c.setAutoCommit(true);
            return u;
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return u;
    }
    
    @Override
    public User getUserByUserID(String userid) {
        Connection c = null;
        User u = null;

        try {
            c = this.connectionUtil.newConnection();
            c.setAutoCommit(false);

            String sql = "Select userid, firstname, lastname, username, pw from users where userid = ?";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, userid);

            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
                u = new User();
                u.setUserId(rs.getInt("userid"));
                u.setFirstname(rs.getString("firstname"));
                u.setLastname(rs.getString("lastname"));
                u.setUsername(rs.getString("username"));
                u.setPassword(rs.getString("pw"));
            }

            c.commit();
            c.setAutoCommit(true);
            return u;
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return u;
    }

    @Override
    public User getUserByUserFirstName(String firstname) {
        Connection c = null;
        User u = null;

        try {
            c = this.connectionUtil.newConnection();
            c.setAutoCommit(false);

            String sql = "Select userid, firstname, lastname, username, pw from users where firstname = ?";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, firstname);

            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
                u = new User();
                u.setUserId(rs.getInt("userid"));
                u.setFirstname(rs.getString("firstname"));
                u.setLastname(rs.getString("lastname"));
                u.setUsername(rs.getString("username"));
                u.setPassword(rs.getString("pw"));
            }

            c.commit();
            c.setAutoCommit(true);
            return u;
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return u;
    }
    
    @Override
    public void newUser(User user) {
    	
    	//logger.info("Debugging... Log...");

        Connection c = null;
try {
        try {
            c = connectionUtil.newConnection();
            c.setAutoCommit(false);
            String sql = "Insert into users (userid, firstname, lastname, username, pw) values (?, ?, ?, ?, ?)";

            PreparedStatement ps = c.prepareStatement(sql);

            ps.setInt(1, user.getUserId());
            ps.setString(2, user.getFirstname());
            ps.setString(3, user.getLastname());
            ps.setString(4, user.getUsername());
            ps.setString(5, user.getPassword());


            ps.executeUpdate(); // executeUpdate returns the number of rows affected

            c.commit();
            c.setAutoCommit(true);

        } catch (SQLException e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            if(c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        
        try {
            c = connectionUtil.newConnection();
            c.setAutoCommit(false);
            String sql = "Insert into accounts (userid, firstname, lastname, username, pw) values (?, ?, ?, ?, ?)";

            PreparedStatement ps = c.prepareStatement(sql);

            ps.setInt(1, user.getUserId());
            ps.setString(2, user.getFirstname());
            ps.setString(3, user.getLastname());
            ps.setString(4, user.getUsername());
            ps.setString(5, user.getPassword());


            ps.executeUpdate(); // executeUpdate returns the number of rows affected

            c.commit();
            c.setAutoCommit(true);

        } catch (SQLException e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            if(c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        
        try {
            c = connectionUtil.newConnection();
            c.setAutoCommit(false);
            String sql = "Insert into customeraccounts (userid, accountid) values (?, ?)";

            PreparedStatement ps = c.prepareStatement(sql);

            ps.setInt(1, user.getUserId());
            ps.setInt(2, Account.getAccountId());



            ps.executeUpdate(); // executeUpdate returns the number of rows affected

            c.commit();
            c.setAutoCommit(true);

        }
        
        catch (SQLException e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        } finally {
            if(c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        
    } finally {
        if(c != null) {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    }
}
