package com.criff.repository;


import java.util.List;

import com.criff.model.Account;

public interface AccountRepository {
    List<Account> getAllAccounts (int userId);
    Account getById(int id);
    double updateBalance(Account a);
}
