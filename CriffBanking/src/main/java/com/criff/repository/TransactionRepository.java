package com.criff.repository;


import java.util.List;

import com.criff.model.Transaction;

public interface TransactionRepository {
    List<Transaction> getAllTransactions (int accountId);
    void newTransaction(Transaction t);
}
